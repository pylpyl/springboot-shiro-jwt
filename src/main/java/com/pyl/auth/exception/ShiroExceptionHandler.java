package com.pyl.auth.exception;

import com.pyl.auth.common.ResponseUtil;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author 灵魂贰婶
 * @version 1.0
 * @date 2023/10/26 14:32
 */
@RestControllerAdvice
public class ShiroExceptionHandler {

    @ExceptionHandler(ShiroException.class)
    public Object unauthenticatedHandler(ShiroException e) {
        if (e instanceof AuthenticationException) {
            return ResponseUtil.fail(500, "认证异常 请联系管理员");
        }
        if (e instanceof AuthorizationException) {
            return ResponseUtil.fail(500, "授权异常 请联系管理员！");
        }
        return ResponseUtil.fail(500, e.getMessage());
    }

    /**
     * 获取堆栈信息
     *
     * @param throwable
     * @return
     */
    public static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }

}
