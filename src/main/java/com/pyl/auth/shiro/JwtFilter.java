package com.pyl.auth.shiro;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.pyl.auth.common.ResponseUtil;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.http.HttpStatus;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 灵魂贰婶
 * @version 1.0
 * @date 2023/10/25 15:47
 */
public class JwtFilter extends BasicHttpAuthenticationFilter {

    public static String LING_TOKEN = "ling_token";


    @Override
    protected String getAuthzHeader(ServletRequest request) {
        System.out.println("-------------------------getAuthzHeader-------------------------");
        HttpServletRequest httpRequest = WebUtils.toHttp(request);
        return httpRequest.getHeader(LING_TOKEN);
    }

    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        System.out.println("-------------------------createToken-------------------------");
        HttpServletRequest httpServletRequest = WebUtils.toHttp(request);
        String token = httpServletRequest.getHeader(LING_TOKEN);
        return StrUtil.isNotBlank(token) ? new JwtToken(token) : null;
    }


    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        System.out.println("-------------------------isAccessAllowed-------------------------");
        try {
            return executeLogin(request, response);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        System.out.println("-------------------------onAccessDenied-------------------------");
        HttpServletResponse httpServletResponse = WebUtils.toHttp(response);
        httpServletResponse.setHeader("Content-Type", "application/json;charset=UTF-8");
        httpServletResponse.setStatus(HttpStatus.OK.value());
        httpServletResponse.getWriter().print(JSONUtil.toJsonStr(ResponseUtil.fail(403, "访问失败，请重新登录!")));
        return false;
    }
}
