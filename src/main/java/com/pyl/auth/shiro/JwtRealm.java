package com.pyl.auth.shiro;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.pyl.auth.controller.AuthController;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author 灵魂贰婶
 * @version 1.0
 * @date 2023/10/25 9:50
 */
@Component
public class JwtRealm extends AuthorizingRealm {

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 身份授权 要进入 jwt 过滤器的请求 有权限注解 全都要走 身份授权
     *
     * @param principalCollection the primary identifying principals of the AuthorizationInfo that should be retrieved.
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        // 就是使用的token
        System.out.println("权限验证---->" + JSONUtil.toJsonStr(principalCollection));
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        Set<String> stringPermissions = new HashSet<>();
//        stringPermissions.add("*"); // admin 具有所有权限
        stringPermissions.add("pingTest:add");

        // TODO 权限没有缓存 可查询 对应redis 进行权限获取


        info.setStringPermissions(stringPermissions);
        return info;
    }

    /**
     * 身份验证 要进入 jwt 过滤器的请求 全都要走 身份验证
     *
     * @param authenticationToken the authentication token containing the user's principal and credentials.
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("身份验证---->" + JSONUtil.toJsonStr(authenticationToken));
        String jws = authenticationToken.getPrincipal().toString();
        SecretKey secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(AuthController.key));
        Jws<Claims> claimsJws = Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(jws);
        Claims payload = claimsJws.getPayload();

        // TODO 可针对 token 放入 redis进行缓存 对线上登录的用户进行 操作： 控制下线，单人登录，等具体线上用户操作

        String id = payload.getId(); // 用户id
        String subject = payload.getSubject(); // 用户账号
        Date issuedAt = payload.getIssuedAt();  // 授权时间
        System.out.println("jwt解码-----》" + JSONUtil.toJsonStr(claimsJws));
        System.out.println(StrUtil.format("用户id:{} 用户账号:{} 签发时间:{}", id, subject, DateUtil.formatDateTime(issuedAt)));

        return new SimpleAuthenticationInfo(authenticationToken.getPrincipal(), authenticationToken.getCredentials(), getName());
    }
}
