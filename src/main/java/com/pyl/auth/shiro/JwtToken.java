package com.pyl.auth.shiro;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;
import org.springframework.stereotype.Component;

/**
 * @author 灵魂贰婶
 * @version 1.0
 * @date 2023/10/25 11:14
 */
@Data
@Component
public class JwtToken implements AuthenticationToken {

    private String jwtToken;

    public JwtToken() {
    }

    public JwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    @Override
    public Object getPrincipal() {
        return getJwtToken();
    }

    @Override
    public Object getCredentials() {
        return getJwtToken();
    }
}
