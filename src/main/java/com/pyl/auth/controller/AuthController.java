package com.pyl.auth.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.pyl.auth.common.ResponseUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.SecretKey;
import java.util.Date;

/**
 * @author 灵魂贰婶
 * @version 1.0
 * @date 2023/10/26 15:25
 */
@RestController
public class AuthController {

    public static final String key = "TatBZKh7S2PmvZhgU5uy9jAeLAIHBrfwuotPcbLB/Es=";

    @GetMapping("/pingTest2")
    public String pingTest2() {
        SecurityUtils.getSubject().checkPermission("pingTest:delete");
        System.out.println("-------------------------pingTest2 需要认证 -------------------------");
        return "pingTest2";
    }

    @GetMapping("/pingTest1")
    public String pingTest1() {

        System.out.println("-------------------------pingTest1 anon -------------------------");
        return "pingTest1";
    }

    @GetMapping("/pingTest")
    @RequiresPermissions({"pingTest:select"})
    public String pingTest() {
        System.out.println("-------------------------pingTest 校验授权 pingTest:select -------------------------");
        return "pongTest";
    }

    @GetMapping("/pingTest3")
    @RequiresPermissions({"pingTest:add"})
    public String pingTest3() {
        System.out.println("-------------------------pingTest3 校验授权 pingTest:add -------------------------");
        return "pingTest3";
    }

    @GetMapping("/login")
    public Object login() {

        SecretKey secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(key));
        // username password 进行登录 生成 token
        String jws = Jwts.builder()
                .id("id")
                .subject("account")
                .issuedAt(DateUtil.date())
                .signWith(secretKey)
                .compact();
        return ResponseUtil.ok(jws);
    }

    private void test() {
        // 自定义密码
//        SecretKey key1 = Jwts.SIG.HS256.key().build();
//        String secretString1 = Encoders.BASE64.encode(key1.getEncoded());
//        System.out.println("jwt密钥 保存到程序 ---》" + secretString1);

        SecretKey secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(key));

        String jws = Jwts.builder()
                .id("id")
                .subject("account")
                .issuedAt(DateUtil.tomorrow())
//                .expiration(DateUtil.yesterday()) // 如果添加了 失效时间则 到期后进行签名直接报错
                .signWith(secretKey)
                .compact();
        System.out.println("jwt编码-----》" + jws);
//        jws = "hahahahahaaaa";
        try {

            // 解密密码后 用于 jwt解密
            Jws<Claims> claimsJws = Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(jws);
            Claims payload = claimsJws.getPayload();
            String id = payload.getId();
            String subject = payload.getSubject();
            Date issuedAt = payload.getIssuedAt();
            System.out.println("jwt解码-----》" + JSONUtil.toJsonStr(claimsJws));
            System.out.println(StrUtil.format("用户id:{} 用户账号:{} 签发时间:{}", id, subject, DateUtil.formatDateTime(issuedAt)));
            //OK, we can trust this JWT

        } catch (JwtException e) {
            System.out.println("jwt解密失败，不是合法的签名");
            e.printStackTrace();
            //don't trust the JWT!
        }
    }

}
