package com.pyl.auth.aop;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.json.JSONUtil;
import com.pyl.auth.exception.ShiroExceptionHandler;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 灵魂贰婶
 * @version 1.0
 * @date 2023/10/26 15:32
 */
@Aspect
@Component
public class RequestLogAspect {

    @Pointcut("(execution(* com.pyl.auth..*.*Controller.*(..)))")
    public void pointCut() {
    }

    /**
     * 备注： 当方法抛出异常，该方法不会执行
     *
     * @param point
     * @return
     */
    @SneakyThrows
    @Around(value = "pointCut()")
    public Object aroundSaveLog(ProceedingJoinPoint point) {
        DateTime start = DateUtil.date();
        // 执行方法
        Object result;
        try {
            result = point.proceed();
            printLog(point, null, start);
        } catch (Throwable e) {
            printLog(point, e, start);
            throw e;
        }
        return result;
    }

    private void printLog(ProceedingJoinPoint point, Throwable e, DateTime start) {
        // 类名称
        String className = point.getTarget().getClass().getSimpleName();
        MethodSignature signature = (MethodSignature) point.getSignature();
        // 函数名称
        String name = signature.getMethod().getName();
        // 参数名
        String[] argNames = signature.getParameterNames();
        //参数值
        Object[] args = point.getArgs();
        Map<String, Object> map = new HashMap<>(15);
        for (int i = 0; i < argNames.length; i++) {
            String argName = argNames[i];
            Object arg = args[i];
            map.put(argName, arg);
        }
        String format = "";
        try {
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            assert requestAttributes != null;
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            if (ObjectUtil.isNotNull(request)) {
                //客户端 请求方式
                String methodType = request.getMethod();
                //客户端 请求url
                String requestURI = request.getRequestURI();

                //客户端 请求客户端终端信息
                String userAgent = request.getHeader(Header.USER_AGENT.getValue());
                format = StrUtil.format(" 请求方式:{} 请求链接:{}  userAgent:{}", methodType, requestURI, userAgent);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        format = StrUtil.format("{} 执行类名称:{} 执行方法:{} 请求参数:{} ", format, className, name, JSONUtil.toJsonStr(map));
        if (ObjectUtil.isNotNull(e)) {
            String stackTrace = ShiroExceptionHandler.getStackTrace(e);
            format = StrUtil.format("{} 异常信息:{}", format, stackTrace);
        }
        DateTime end = DateUtil.date();
        long between = DateUtil.between(start, end, DateUnit.MS);
        format = StrUtil.format(" {} 耗时:{}ms", format, between);
        System.out.println(format);
    }

}
