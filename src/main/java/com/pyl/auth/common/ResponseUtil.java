package com.pyl.auth.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 灵魂贰婶
 * @version 1.0
 * @date 2023/10/26 11:31
 */
public class ResponseUtil {

    public static Object ok() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", 0);
        obj.put("success", true);
        obj.put("errmsg", "成功");
        return obj;
    }

    public static Object ok(Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", 0);
        obj.put("success", true);
        obj.put("errmsg", "成功");
        obj.put("data", data);
        return obj;
    }

    public static Object ok(int errno, String errmsg, Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", errno);
        obj.put("success", true);
        obj.put("errmsg", errmsg);
        obj.put("data", data);
        return obj;
    }

    public static Object common(int errno, boolean success, String errmsg, Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", errno);
        obj.put("success", success);
        obj.put("errmsg", errmsg);
        obj.put("data", data);
        return obj;
    }

    public static Object common(int errno, String errmsg, boolean success) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", errno);
        obj.put("success", success);
        obj.put("errmsg", errmsg);
        return obj;
    }

    public static Object ok(String errmsg, Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", 0);
        obj.put("success", true);
        obj.put("errmsg", errmsg);
        obj.put("data", data);
        return obj;
    }

    public static Object ok(int errno, String errmsg) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", errno);
        obj.put("success", true);
        obj.put("errmsg", errmsg);
        return obj;
    }


    public static Object fail() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", -1);
        obj.put("success", false);
        obj.put("errmsg", "错误");
        return obj;
    }

    public static Object fail(int errno, String errmsg) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("errno", errno);
        obj.put("success", false);
        obj.put("errmsg", errmsg);
        return obj;
    }

    public static Object fail(int errno, String errmsg, String data) {
        Map<String, Object> obj = new HashMap<String, Object>(3);
        obj.put("errno", errno);
        obj.put("success", false);
        obj.put("errmsg", errmsg);
        obj.put("data", data);
        return obj;
    }

    public static Object fail(int errno, String errmsg, Object data) {
        Map<String, Object> obj = new HashMap<String, Object>(3);
        obj.put("errno", errno);
        obj.put("success", false);
        obj.put("errmsg", errmsg);
        obj.put("data", data);
        return obj;
    }

    public static Object badArgument() {
        return fail(401, "参数不对");
    }

    public static Object badArgumentValue() {
        return fail(402, "参数值不对");
    }

    public static Object unlogin() {
        return fail(501, "请登录");
    }

    public static Object serious() {
        return fail(502, "系统繁忙,请稍后再试!");
    }

    public static Object unsupport() {
        return fail(503, "业务不支持");
    }

    public static Object updatedDateExpired() {
        return fail(504, "更新数据已经失效");
    }

    public static Object updatedDataFailed() {
        return fail(505, "更新数据失败");
    }

    public static Object unauthz() {
        return fail(506, "无操作权限");
    }

}
