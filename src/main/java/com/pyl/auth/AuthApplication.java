package com.pyl.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(cn.hutool.extra.spring.SpringUtil.class) // 使用hutool 的 spring工具类
public class AuthApplication {


    public static ConfigurableApplicationContext run;

    public static void main(String[] args) {
        run = SpringApplication.run(AuthApplication.class, args);
    }


}
